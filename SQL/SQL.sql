create database web_lottery default character set utf8;


create table user(
id int auto_increment not null primary key,
mailaddress varchar(255) not null unique,
password varchar(255) not null,
nickname varchar(255) not null,
face_img_path varchar(255) not null unique);

drop table review_data;


create table store(
id int auto_increment primary key,
mailaddress varchar(255) not null unique,
password varchar(255) not null,
store_name varchar(255) not null unique,
store_street_address varchar(255) not null unique,
map_img_path varchar(255) not null unique,
store_img_path varchar(255) not null unique,
start_business_hours time not null,
end_business_hours time not null,
20s boolean,
5s boolean,
2s boolean,
4p boolean,
2p boolean,
1p boolean,
number_of_20s int,
number_of_5s int,
number_of_2s int,
number_of_4p int,
number_of_2p int,
number_of_1p int);


create table lottery_result(
id int auto_increment primary key,
store_id int not null,
user_id int not null,
lottery_number int not null,
lottery_date date not null);

create table user_fabo(
id int auto_increment primary key,
user_id int not null,
store_id int not null);

create table inquiry_data(
id int auto_increment primary key,
store_id int not null,
user_id int not null,
kind_of_inquiry int not null,
inquiry_content text not null,
send_date date not null);


create table review_data(
id int auto_increment primary key,
store_id int not null,
user_id int not null,
num_star int not null,
review_content text not null,
send_date date not null);
